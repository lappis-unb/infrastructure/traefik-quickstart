# Traefik Quickstart

Notas sobre setup do Traefik seguindo o tutorial Quickstart oficial do link
abaixo.

* https://docs.traefik.io/

Ambiente utilizado:

* Debian 9 numa VM VirtualBox
  * IP: 192.168.56.31
* Docker versão 18.06.1
* Docker Compose versão 1.23.2

Para os testes adicione os hosts que iremos utilizar no seu `/etc/hosts`
adicionando a seguinte linha:

```
192.168.56.31 docker.localhost whoami.docker.localhost nginx.docker.localhost cheddar.docker.localhost httpd.docker.localhost
```

## Conceitos básicos do Traefik

### Entrypoint

Entrypoints são pontos de entrada de rede para o Traefik, usualmente definem
portas de entrada como protocolos HTTP ou HTTPS.

### Frontend

Frontends consiste de um conjunto de regras que determinam como requisições que
chegam nos entrypoints serão encaminhadas para os backends.

### Backends

Backends são responsáveis por receber e responder de volta as requesições que
chegam, neste exemplo são os containers Docker, mas pode ser utilizado em
conjunto com orquestradores como Kubernetes por exemplo.

## 0. Setup

Criar rede `traefik` no Docker:

```console
docker network create traefik
```

## 1. Instalar Traefik

```console
docker-compose up -d traefik
```

O resultado deve ser o seguinte:

![install traefik](screenshots/install-traefik.png)

O dashboard do Traefik deve estar disponível na porta `8080`, acesse o link abaixo.

* http://192.168.56.31:8080

O dashboard deve ser algo parecido com a imagem abaixo.

![traefik dashboard](screenshots/traefik-dashboard.png)

## 2. Instalar Serviços

```console
docker-compose up -d whoami
```

O resultado do comando acima deve ser algo similar ao seguinte:

![install whoami service](screenshots/install-whoami.png)

O novo serviço pode ser visualizado no dashboard do traefik como na imagem
abaixo.

![service whoami no dashboard](screenshots/whoami-dashboard.png)

Acesse http://whoami.docker.localhost. O serviço `whoami` imprime informações
da requisição, algo semelhante à imagem abaixo.

![service whoami screenshot](screenshots/whoami.png)

## 3. Aumentar Instâncias dos Serviços

```console
docker-compose up --scale whoami=2 -d whoami
```

A saída do comando acima deve ser algo semelhante a imagem abaixo.

![scale service whoami](screenshots/scale-whoami.png)

É possível verificar no dashboard o resultado agora com dois backends para este
serviço.

![scale service whoami dashboard](screenshots/scale-whoami-dashboard.png)

Atualize (F5) a página http://whoami.docker.localhost e note que o Traefik
agora balanceia o acesso entre os dois backends http://172.18.0.3:80 e
http://172.18.0.4:80

## 4. Instalar outro Serviço

```console
docker-compose up -d nginx
```

![install service nginx](screenshots/install-nginx.png)

Veja o resultado acessando http://nginx.docker.localhost

O trecho relacionado a este serviço Nginx no YAML do Docker Compose
`docker-compose.yml` é o seguinte:

```yaml
  # Service nginx
  nginx:
    image: nginx:1.13.8
    labels:
      - "traefik.frontend.rule=Host:nginx.docker.localhost"
    networks:
      - traefik
```

## 5. Instalar Serviço sem o uso do docker-compose

É possível executar um container diretamente pela linha de comando passando os
parâmetros corretos para o Traefik identificar o container e criar a rota
adequada.

```console
docker run --network traefik --label "traefik.frontend.rule=Host:cheddar.docker.localhost" -d errm/cheese:cheddar
```

![install chedar container](screenshots/install-cheddar.png)

Acesse http://cheddar.docker.localhost o resultado deve ser algo como no
screenshot abaixo.

![cheddar browser](screenshots/cheddar.png)

## 6. Rotear serviços usando Path ao invés de Host

O Traefik suporta uma infidade de estratégias para roteamento, uma delas é
através de combinação exata via `PathStrip`. A diferença entre `Path` e
`PathStrip` é que o Traefik recebe a requisição com o path e remove o path ao
repassar a requisiçao apra o backend, geralmente este é o comportamento
desejado.

Melhor ainda é usar o `PathPrefixStrip` pois o `PathStrip` aceita apenas
requisições neste path específico, qualquer sub-path a partir deste não será
roteado pelo Traefik, exemplo se houver imagens ou css sendo referenciados não
estarão disponíveis.

```console
docker-compose up -d stilton
```

Veja a configuração para a URL http://docker.localhost/stilton/ no arquivo
`docker-compose.yml` neste repositório, copiado abaixo:

```yaml
  # Service stilton PATH
  stilton:
    image: errm/cheese:stilton
    labels:
      - "traefik.frontend.rule=Host:docker.localhost;PathPrefixStrip:/stilton/"
    networks:
      - traefik
```

O resultado deve ser algo como na imagem abaixo.

![stilton browser](screenshots/stilton.png)

## 7. Instalar Serviço com Dockerfile

É possível passar os labels necessários ao Traefik via Dockerfile também, veja
o arquivo `Dockerfile` deste repositório, nele buildamos uma imagem a partir do
Apache `httpd`.

```console
docker build -t traefik/httpd .
```

Veja a imagem construída:

```console
docker image ls
```

Executar a imagem:

```console
docker run --net=traefik traefik/httpd
```

Acesse http://httpd.docker.localhost

# Problemas

## 504 Gateway Timeout

Ao acessar o serviço `whoami` no hostname `whoami.docker.localhost` recebo
o erro Gateway Timeout, os logs do Traefik apresentam a seguinte mensagem.

```
"'504 Gateway Timeout' caused by: dial tcp 172.20.0.2:80: i/o timeout"
```

Para acessar os logs do Traefik basta subir os serviços em primeiro plano omitindo o parâmetro `-d`.

```console
docker-compose up
```

Os links reportam problemas relacionados:

* https://github.com/containous/traefik/issues/994
* https://github.com/containous/traefik/issues/1254
* https://github.com/containous/traefik/issues/4297
* https://www.reddit.com/r/Traefik/comments/924wqq/getting_gateway_timeouts_from_traefik/

**Solução:**

Criar uma rede chamada `traefik` no Docker:

```console
docker network create traefik
```

Configurar rede `traefik` entre os containers na configuração do
docker-compose, exemplo abaixo:

```yaml
version: '3'
services:
  reverse-proxy:
    image: traefik
    command: - --api - --docker
    ports:
      - "80:80"
      - "8080:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    networks:
      - traefik
networks:
  traefik:
    external: true
```

## 404 Not Found

Configurar uma rota via Path ao invés de Host com o container de teste
`err/cheese:stilton` dá o seguinte erro na imagem abaixo.

![404 stilton not found](screenshots/stilton-404.png)

Deacordo com a documentação do Traefik requisições usando a configuração `Path`
repassa exatamente o path vindo da requisição para o backend e caso o backend
não responda neste path acontece o erro relatado aqui.

**Solução:**

Usar a configuração `PathStrip` ou `PathPrefixStrip` do Traefik ao invés da
config `Path`.

Consulte a documentação https://docs.traefik.io/basics/ para mais detalhes
sobre estes parâmetros.
