FROM httpd:alpine
LABEL traefik.frontend.rule="Host:httpd.docker.localhost"
EXPOSE 80
